#!/bin/bash
# vim: sts=4 sw=4 et

# References:
# <https://wiki.debian.org/PbuilderTricks>
# <https://wiki.debian.org/cowbuilder>
# <https://help.launchpad.net/Packaging/PPA/BuildingASourcePackage>

set -e
set -u

GOODVIBES_DIR=  # $1

# DIST    SUFFIX BUILD-TYPE
BUILD_SPECS="\
unstable  -      all"

BUILDER=sbuild    # pbuilder not tested anymore
UPDATE_BUILDENV=1

SIGNING_KEY=      # to sign releases

# helpers

fail() {
    echo >&2 $@
    exit 1
}

title() {
    echo -e '\e[7m'"⏵ $@"'\e[0m'
}

usage() {
    local status=${1:-2}

    [ $status -eq 0 ] || exec >&2

    echo "Usage: $(basename $0) snapshot/release [ARGUMENTS]"
    echo
    echo "This script builds goodvibes packages for Debian/Ubuntu."
    echo
    echo "Arguments:"
    echo "  snapshot GOODVIBES_DIR"
    echo "  release  <no argument>"
    echo

    exit $status
}

# git helpers

git_on_branch() {
    local branch=$1

    test "$(git rev-parse --abbrev-ref HEAD)" = "$branch"
}

git_working_tree_clean() {
    git diff-index --quiet HEAD --
}

git_make_archive() {
    local branch=$1
    local format=$2
    local output=$3
    local prefix=$(basename $(pwd))/    # needs ending slash

    git archive --prefix=$prefix --format=$format -o $output $branch
}

# pbuilder helpers

PBUILDER_CACHE=/var/cache/pbuilder

pbuilder_checkdist() {
    local dist=$1
    local chrootdir=$PBUILDER_CACHE/base-$dist.cow

    test -d $chrootdir
}

pbuilder_update() {
    local dist=$1
    local chrootdir=$PBUILDER_CACHE/base-$dist.cow

    sudo cowbuilder update \
        --distribution $dist \
        --basepath $chrootdir
}

pbuilder_build_binary() {
    local dist=$1
    local chrootdir=$PBUILDER_CACHE/base-$dist.cow
    local opts=()

    [ "$SIGNING_KEY" ] && \
        opts+=(--auto-debsign)

    DIST=$dist \
    pdebuild --pbuilder cowbuilder \
	     --buildresult .. \
	     --debbuildopts "--build=full --no-pre-clean" \
             ${opts[@]} \
	     -- \
	     --basepath $chrootdir \
	     --distribution $dist
}

# sbuild helpers

sbuild_checkdist() {
    local dist=$1

    schroot -l | grep -q chroot:$dist-amd64-sbuild
}

sbuild_update() {
    local dist=$1

    sudo sbuild-update -udcar \
        $dist-amd64-sbuild
}

sbuild_build() {
    local dist=$1
    local opts=()

    opts+=(-d $dist)    # --dist
    opts+=(-s)          # --source
    [ "$SIGNING_KEY" ] && \
        opts+=(-k $SIGNING_KEY)    # --keyid

    sbuild ${opts[@]}
}

sbuild_build_binary() {
    local dist=$1

    sbuild -d $dist
}

# generic build helpers

checkdist() {
    case $BUILDER in
        (sbuild)   sbuild_checkdist $@    ;;
        (pbuilder) pbuilder_checkdist $@  ;;
	(*)        fail "Unknown builder" ;;
    esac
}

update_buildenv() {
    case $BUILDER in
        (sbuild)   sbuild_update $@       ;;
        (pbuilder) pbuilder_update $@     ;;
	(*)        fail "Unknown builder" ;;
    esac
}

build_all_package() {
    case $BUILDER in
        (sbuild)   sbuild_build $@        ;;
        (pbuilder) fail "Not implemented" ;;
        (*)        fail "Unknown builder" ;;
    esac
}

build_binary_package() {
    case $BUILDER in
        (sbuild)   sbuild_build_binary $@   ;;
        (pbuilder) pbuilder_build_binary $@ ;;
        (*)        fail "Unknown builder"   ;;
    esac
}

build_source_package() {
    local opts=()

    opts+=(-S)     # --build=source
    opts+=(-sa)    # forces the inclusion of the original source
    opts+=(-i)     # --diff-ignore
    opts+=(-nc)    # --no-pre-clean
    if [ -z "$SIGNING_KEY" ]; then
        opts+=(--no-sign)    # do not sign any file
    fi
    # I wished to use -k $SIGNING_KEY to be explicit about the key,
    # but it doesn't seem to work. Both "-k $KEY" and "-k=$KEY" fail,
    # for different reasons. Time to give up.

    dpkg-buildpackage ${opts[@]}
}

build_package() {
    local build_type=$1
    shift

    case $build_type in
        (all)    build_all_package $@      ;;
        (binary) build_binary_package $@   ;;
        (source) build_source_package $@   ;;
        (*)      fail "Unknown build type" ;;
    esac
}

# more helpers

get_gpg_keyid() {
    [ "${DEBEMAIL:-}" ] || fail "DEBEMAIL is not set, can't guess the GPG key id"
    gpg --with-colons --list-key "$DEBEMAIL" | grep ^pub | cut -d: -f5
}

mk_snapshot_version() {
    local repodir=$1
    local sha=
    local date=
    local lasttag=
    local version=

    pushd $repodir >/dev/null

    git_on_branch "master" || fail "Not on master branch"
    git_working_tree_clean || fail "Working tree '$(pwd)' dirty"

    export TZ=UTC # to get commit date in utc
    date=$(git show --no-patch --format=%cd --date=format-local:%Y%m%d HEAD)
    unset TZ
    sha=$(git rev-parse --short HEAD)
    lasttag=$(git describe --tags $(git rev-list --tags --max-count=1))
    version=$(echo "${lasttag}+git${date}.${sha}" | sed 's/^v//')
    # we end up with something like 0.4.3+git20190922.54b9304

    popd >/dev/null

    echo $version
}

mk_snapshot_tarball() {
    local repodir=$1
    local format=$2
    local output=$3

    pushd $repodir >/dev/null

    git_on_branch "master" || fail "Not on master branch"
    git_working_tree_clean || fail "Working tree '$(pwd)' dirty"
    git_make_archive "master" "$format" "$output"

    popd >/dev/null
}

adjust_suffix() {
    local suffix=$1

    if [ "$suffix" = "-" ]; then
        suffix=
    else
        suffix="~${suffix}u"
    fi

    echo $suffix
}

set_section() {
    local section=$1

    # beware of slashes in section name
    sed -i "s;^Section: .*;Section: $section;" debian/control
}

set_changelog() {
    local dist=$1
    local suffix=$2
    
    # unrelease, so that dch does not create a new entry
    sed -i '1!b;s/) .*;/) UNRELEASED;/' debian/changelog

    # append suffix to version
    if [ "$suffix" ]; then
        dch -l "$suffix" ""
    fi

    # release with distribution
    [ "$dist" == "sid" ] && dist=unstable
    dch --distribution $dist --release ""
}

reset_changelog() {
    local dist=$1
    local suffix=$2
    local orig_dist=$3

    # remove suffix from version
    if [ "$suffix" ]; then
        sed -i '1!b;'"s/${suffix}1//" debian/changelog
    fi

    # reset distribution
    sed -i '1!b;'"s/) .*;/) ${orig_dist};/" debian/changelog
}

# main

[ -d debian ] || fail "Please run from root directory of the project"

[ $# -eq 0 ] && usage 0

while [ "$#" -gt 0 ]; do
    case "$1" in
        (-h|--help)
            usage 0
            ;;
        (snapshot)
            shift
            [ "${1:-}" ] || usage 1
            GOODVIBES_DIR=$1
            shift
            ;;
        (release)
            shift
            ;;
        (*)
            usage 1
            ;;
    esac
done

[ $# -eq 0 ] || usage 1

if [ "$GOODVIBES_DIR" ]; then

    # we build a snapshot, hence we'll bump the version in d/changelog,
    # and create upstream tarball from a given directory

    [ -d "$GOODVIBES_DIR" ] || fail "'$GOODVIBES_DIR' is not a directory"

    git_working_tree_clean || fail "Working tree '$(pwd)' dirty"

    UVERSION=$(mk_snapshot_version "$GOODVIBES_DIR")
    DVERSION=$UVERSION-0goodvibes1

    title "Making an orig tarball for snapshot $UVERSION ..."
    mk_snapshot_tarball "$GOODVIBES_DIR" "tar.gz" "$(pwd)/../goodvibes-$UVERSION.tar.gz"
    dch --newversion $DVERSION "Bump changelog for snapshot $DVERSION"
    mk-origtargz ../goodvibes-$UVERSION.tar.gz
    origtargz --unpack

    # we don't sign snapshots
    SIGNING_KEY=

else

    # we build a release, hence assume that d/changelog was bumped already,
    # and we download upstream tarball from the web

    DVERSION=$(dpkg-parsechangelog --show-field version)
    UVERSION=$(echo $DVERSION | rev | cut -d- -f2- | rev)

    title "Downloading orig tarball for current version '$UVERSION' ..."
    uscan --verbose --download-current-version
    origtargz --unpack

    # we sign releases
    SIGNING_KEY=$(get_gpg_keyid)
fi

ORIG_DIST=$(dpkg-parsechangelog --show-field distribution)
ORIG_SECTION=$(sed -n 's/^Section: *//p' debian/control)

set_section goodvibes/$ORIG_SECTION

while read -r dist suffix build_type; do
    # Need a build environment, unless we only build source package
    if [ "$build_type" != source ]; then
        if ! checkdist $dist; then
            title "No build environment: dist=$dist. Skipping."
	    continue
        fi

        if [ $UPDATE_BUILDENV -eq 1 ]; then
            title "Updating build environment: dist=$dist ..."
            update_buildenv $dist
        fi
    fi

    # Adjust suffix for d/changelog
    suffix=$(adjust_suffix "$suffix")
        
    # Build
    title "Building package: build-type=$build_type, dist=$dist, suffix=$suffix ..."
    set_changelog "$dist" "$suffix"
    echo "debian/changelog: $(head -1 debian/changelog)"
    build_package "$build_type" "$dist"
    reset_changelog "$dist" "$suffix" "$ORIG_DIST"
done <<< $BUILD_SPECS

set_section $ORIG_SECTION

# Done
title "Done, time to upload!"
dput --host-list
